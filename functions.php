<?php

function getUsers($return_assoc = 0){
 $users = json_decode(file_get_contents(POST_DB) , $return_assoc);
 return (array)$users;
}


function insertUser(stdClass $user)
{
 $users = getUsers(1);
 $users[] = (array)$user;
 $users_json = json_encode($users);
 file_put_contents(POST_DB , $users_json);
 return true;
}



function findUser($email , $password )
{
 $users = getUsers(1);
$finded_user = false;

 foreach($users as $uid => $user){
     $name = $user["display-name"];
     if(($user["email"] == $email) && $user["password"] == $password ){
      $finded_user = $user;
     }
 }
 return $finded_user;
}

function siteUrl($uri = ''){
 return BASE_URl . $uri;
}

?>